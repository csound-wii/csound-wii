# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
PYTHON_DEPEND="2"

inherit python eutils qt4-r2

MY_P="${P/-/}"
DESCRIPTION="PythonQt is a dynamic Python binding for the Qt framework."
HOMEPAGE="http://pythonqt.sourceforge.net/"
SRC_URI="mirror://sourceforge/pythonqt/pythonqt/${P}/${MY_P}.zip"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples"

DEPEND="dev-qt/qtgui:4
	dev-qt/qtxmlpatterns:4"

S="${WORKDIR}/${MY_P}"

pkg_setup() {
	python_set_active_version 2
}

src_prepare() {
	sed -i "s@PYTHON_VERSION=2.6@PYTHON_VERSION=$(python_get_version)@" build/python.prf || die
	sed -i "s@python\$\${PYTHON_VERSION}-config@python-config-\$\${PYTHON_VERSION}@g" build/python.prf || die
}

src_configure() {
	eqmake4
}

src_compile() {
	emake all
}

src_install() {
	insinto /usr/$(get_libdir)
	insopts -m0755
	doins lib/libPythonQt.so.1.0.0 lib/libPythonQt_QtAll.so.1.0.0
	cd ${D}/usr/$(get_libdir)
	ln -s libPythonQt.so.1.0.0 libPythonQt.so.1.0
	ln -s libPythonQt.so.1.0.0 libPythonQt.so.1
	ln -s libPythonQt.so.1.0.0 libPythonQt.so
	ln -s libPythonQt_QtAll.so.1.0.0 libPythonQt_QtAll.so.1.0
	ln -s libPythonQt_QtAll.so.1.0.0 libPythonQt_QtAll.so.1
	ln -s libPythonQt_QtAll.so.1.0.0 libPythonQt_QtAll.so
	cd ${S}
	dobin generator/pythonqt_generator
	if use examples; then
		insinto /usr/share/${P}/examples
		insopts -m0755
		doins lib/CPPPy* lib/Py* tests/PythonQtTest
	fi
	dodoc README
	newdoc CHANGELOG.txt ChangeLog
}
