# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils

DESCRIPTION="Control tool for Envy24 (ice1712) based soundcards."
HOMEPAGE=""
SRC_URI="http://nielsmayer.com/envy24control/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND="media-libs/alsa-lib
	x11-libs/gtk+:2"

DEPEND="${RDEPEND}"

RESTRICT="mirror"

src_compile () {
	emake
}

src_install () {
	newbin envy24control mudita24
	dodoc AUTHORS README
}
