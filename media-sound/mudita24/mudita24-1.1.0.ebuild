# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit eutils cmake-utils

DESCRIPTION="Control tool for Envy24 (ice1712) based soundcards."
HOMEPAGE=""
SRC_URI="http://mudita24.googlecode.com/files/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND="media-libs/alsa-lib
	x11-libs/gtk+:2"

DEPEND="${RDEPEND}
	dev-util/cmake"


RESTRICT="mirror"

