# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
PYTHON_DEPEND="2"

inherit eutils python

DESCRIPTION="Graphical user interface for building csound instruments by patching modules similar to modular synthesizers."
HOMEPAGE="http://cabel.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${PN}/${P}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-python/wxpython
	media-sound/csound"

pkg_setup() {
	python_set_active_version 2
}

src_prepare() {
	python_convert_shebangs -r 2 .
}

src_install() {
	insinto /usr/$(get_libdir)/cabel
	doins -r examples model modules stuff tools view
	doins cabel.py

	echo "cd /usr/$(get_libdir)/cabel" > cabel
	echo "python cabel.py" >> cabel
	dobin cabel

	dodoc AUTHORS ChangeLog README TODO
}
