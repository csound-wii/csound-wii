# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit toolchain-funcs

MY_P="${P/0.4/v0.4}"
DESCRIPTION="Nintendo Wiimote to OSC converter."
HOMEPAGE="http://www.nescivi.nl/wiiosc/"
SRC_URI="http://www.nescivi.nl/wiiosc/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="media-libs/liblo
	app-misc/cwiid"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	sed -i "s@CC:=gcc@CC:=$(tc-getCC)@" Makefile
	sed -i "s@^CFLAGS@# CFLAGS@" Makefile
}

src_compile() {
	export CFLAGS
	emake
}

src_install() {
	dobin wiiosc
	dodoc README
}
