# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
PYTHON_DEPEND="2"

inherit python eutils distutils

MY_P="${P/0_alpha15/0a15}"
DESCRIPTION="Modular poly-paradigm algorithmic music composition in an interactive command-line environment."
HOMEPAGE="http://www.flexatone.net/athena.html"
SRC_URI="http://athenacl.googlecode.com/files/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND="media-sound/csound[csoundac,python]"

S="${WORKDIR}/${MY_P}"

pkg_setup() {
	python_set_active_version 2
}

src_prepare() {
	python_convert_shebangs -r 2 .
	epatch ${FILESDIR}/doc.patch
}

src_install() {
	distutils_src_install
	newdoc athenaCL/doc/HISTORY.txt ChangeLog
	newdoc athenaCL/doc/README.txt README
	if use doc; then
		dohtml -r athenaCL/doc/html/*
		insinto /usr/share/doc/${PF}
		doins athenaCL/doc/athenaclManual.htm
	fi
}
