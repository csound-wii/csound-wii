# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils toolchain-funcs

MY_PN=${PN/algoscore/AlgoScore}
DESCRIPTION="AlgoScore is a graphical environment for algorithmic composition."
HOMEPAGE="http://kymatica.com/Software/AlgoScore"
SRC_URI="http://download.gna.org/algoscore/${MY_PN}-${PV}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RDEPEND="media-sound/jack-audio-connection-kit
	media-sound/csound
	media-libs/libsndfile
	media-libs/liblo
	x11-libs/gtk+:2
	dev-libs/glib:2
	dev-libs/libpcre"

DEPEND="${RDEPEND}
	dev-util/cmake"

S="${WORKDIR}/${MY_PN}"

src_compile() {
	cd src
	export CC="$(tc-getCC)"
	export CXX="$(tc-getCXX)"
	export CFLAGS
	export LDFLAGS
	./make_build
}

src_install() {
	insinto /usr/$(get_libdir)/AlgoScore
	insopts -m0755
	doins algoscore
	insopts -m0644
	doins -r classes examples lib as_icon.png
	echo "cd /usr/$(get_libdir)/AlgoScore" > algoscore-run
	echo "./algoscore" >> algoscore-run
	dobin algoscore-run
	insinto /usr/share/pixmaps
	doins as_icon.png as_icon.svg
	dodoc Help/ChangeLog README TODO
	if use doc; then
		dohtml Help/*.html Help/*.png
	fi
	make_desktop_entry algoscore-run AlgoScore as_icon
}
