# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils

DESCRIPTION="Cecilia is a GUI to csound."
HOMEPAGE="http://cecilia.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${PN}-src/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="sox"

DEPEND="media-sound/csound
	dev-lang/tk
	dev-lang/tcl
	sox? ( media-sound/sox )"

src_compile() {
	einfo "Nothing to compile"
}

src_install() {
	insinto /usr/lib/cecilia
	doins -r files lib
	find ${D} -depth -type d -name "CVS" -exec rm -rf {} \;
	dobin cecilia cecilia-tcl
	dodoc README TODO
	insinto /usr/share/pixmaps
	newins files/graphics/help.gif cecilia.gif
	make_desktop_entry cecilia Cecilia cecilia
}
