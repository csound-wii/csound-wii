# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
WX_GTK_VER="2.8"

inherit wxwidgets

DESCRIPTION="Smasher is an audio loop slicer."
HOMEPAGE="http://smasher.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="x11-libs/wxGTK:2.8[gstreamer]
	media-libs/libsndfile
	media-libs/libmad
	media-sound/csound[double-precision]"

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc AUTHORS README
}
