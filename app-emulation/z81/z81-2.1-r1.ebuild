# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
inherit eutils

DESCRIPTION="z81 is a Sinclair ZX81 (aka TS1000) emulator."
HOMEPAGE="http://rus.members.beeb.net/z81.html"
SRC_URI="ftp://ftp.ibiblio.org/pub/Linux/system/emulators/zx/${P}.tar.gz"

IUSE=""
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"

DEPEND="x11-libs/libX11"
SHAREDIR="/usr/share/z81"
	
src_compile() {
	epatch ${FILESDIR}/${P}-destdirs.patch
	emake xz81 zx81get || die
}

src_install() {
	dodir ${SHAREDIR}
	dobin xz81 zx81get
	cp ${S}/*.pbm ${D}/${SHAREDIR}
	cp -R ${S}/games-etc ${D}/${SHAREDIR}
	cp -R ${S}/saverom ${D}/${SHAREDIR}
	cp ${FILESDIR}/zx81.rom ${D}/${SHAREDIR}
	insinto /usr/share/pixmaps
	doins *.xpm
	doman *.1
	dosym /usr/share/man/man1/z81.1 /usr/share/man/man1/xz81.1
	dodoc ChangeLog GAMENOTES NEWS README
	make_desktop_entry xz81 "Sinclair ZX81 Emulator" xz81.xpm
}
