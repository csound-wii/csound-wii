# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

IUSE=""
SLOT="0"

DESCRIPTION="z81 is a Sinclair ZX81 (aka TS1000) emulator."
HOMEPAGE="http://rus.members.beeb.net/z81.html"

SOURCE_URI="ftp://ftp.ibiblio.org/pub/Linux/system/emulators/zx/${P}.tar.gz"
ROM_URI="ftp://ftp.nvg.ntnu.no/pub/sinclair/roms/zx81.rom"
SRC_URI="${SOURCE_URI} ${ROM_URI}"

# Not sure about the license of the ROM image, so just in case...
RESTRICT="nomirror"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"

DEPEND="x11-base/xorg-x11"
SHAREDIR="/usr/share/z81"
	
src_compile() {
	epatch ${FILESDIR}/${P}-destdirs.patch
	emake xz81 zx81get || die
}

src_install() {
	dodir ${SHAREDIR}
	dobin xz81 zx81get
	cp ${S}/*.pbm ${D}/${SHAREDIR}
	cp -R ${S}/games-etc ${D}/${SHAREDIR}
	cp -R ${S}/saverom ${D}/${SHAREDIR}
	cp ${DISTDIR}/zx81.rom ${D}/${SHAREDIR}
	insinto /usr/share/pixmaps
	doins *.xpm
	doman *.1
	dosym /usr/share/man/man1/z81.1 /usr/share/man/man1/xz81.1
	dodoc ChangeLog GAMENOTES NEWS README
}
