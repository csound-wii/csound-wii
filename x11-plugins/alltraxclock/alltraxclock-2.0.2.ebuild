# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit gkrellm-plugin

DESCRIPTION="Transparent analog clock for GKrellM2"
HOMEPAGE="http://perso.wanadoo.fr/alltrax/alltraxclock.html"
# The upstream is dead, so I'm hosting the sources myself
SRC_URI="http://steam.punk.dy.fi/files/${P}.tar.gz"

SLOT="2"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE=""

PLUGIN_SO="alltraxclock.so"