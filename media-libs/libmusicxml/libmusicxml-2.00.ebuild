# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils toolchain-funcs

MY_P=${P/00/00-src}
DESCRIPTION="MusicXML is a music interchange format designed for notation, analysis, retrieval, and performance applications."
HOMEPAGE="http://libmusicxml.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${MY_P}.tgz"

LICENSE="LGPL-2.1"
SLOT="2"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-util/cmake"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	if use amd64; then
		sed -i "s@DESTINATION lib@DESTINATION lib64@g" cmake/CMakeLists.txt
	fi
}

src_configure() {
	cd cmake
	export CC="$(tc-getCC)"
	export CXX="$(tc-getCXX)"
	cmake \
		-DCMAKE_INSTALL_PREFIX="/usr" \
		-DCMAKE_C_FLAGS="${CFLAGS}" \
		-DCMAKE_CXX_FLAGS="${CXXFLAGS}" \
		-DCMAKE_SHARED_LINKER_FLAGS="${LDFLAGS}" \
		CMakeLists.txt
}

src_compile() {
	cd cmake
	emake
}
src_install() {
	cd cmake
	make DESTDIR="${D}" install
	newdoc ../README.txt README
}
