#! python
# -*- coding: utf-8 -*-
import sys
sys.path.append('../scripting/')
import loris

f = loris.AiffFile('clarinet.aiff')
a = loris.Analyzer(40, 80)
loris.exportSdif('clarinet.sdif', a.analyze(f.samples(), f.sampleRate()))

f = loris.AiffFile('flute.aiff')
a = loris.Analyzer(40, 80)
loris.exportSdif('flute.sdif', a.analyze(f.samples(), f.sampleRate()))
