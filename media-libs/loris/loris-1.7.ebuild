# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
PYTHON_DEPEND="2"

inherit python eutils flag-o-matic

DESCRIPTION="Loris is a library for sound analysis, synthesis, and morphing."
HOMEPAGE="http://www.hakenaudio.com/Loris/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="csound +fftw +python"

DEPEND="dev-lang/swig
	csound? ( >=media-sound/csound-5.12.1-r3 )
	fftw? ( sci-libs/fftw:3.0 )"

pkg_setup() {
	python_set_active_version 2
}

src_configure() {
	if use csound; then
		if has_version media-sound/csound[double-precision]; then
			append-cppflags "-DUSE_DOUBLE -I/usr/include/csound/"
		else
			append-cppflags "-I/usr/include/csound/"
		fi
	fi
	econf \
		$(use_with fftw)\
		$(use_with python) \
		$(use_with csound csound5)
}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc AUTHORS NEWS README THANKS
	if use csound; then
		if has_version media-sound/csound[double-precision]; then
			insinto /usr/$(get_libdir)/csound/plugins64
		else
			insinto /usr/$(get_libdir)/csound/plugins
		fi
		insopts -m0755
		newins src/.libs/libloris.so.12.0.0 libloris.so
	# Convert .aiff to .sdif for use with example programs; Needs a bit of creative
	# shuffling to use a not-yet-installed python module :)
		cd scripting
		cp .libs/_loris.a .
		cp .libs/_loris.so .
		cd ../test
		python ${FILESDIR}/aifftosdif.py
		cd ..
		insinto /usr/share/doc/${PF}/examples
		insopts -m0644
		doins test/clarinet.sdif test/flute.sdif csound/*.csd
	fi
}

pkg_postrm() {
	python_mod_cleanup loris.py
}