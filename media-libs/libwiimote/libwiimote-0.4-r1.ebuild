# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils autotools

DESCRIPTION="Libwiimote is a simple C library for communicating with the Nintendo Wii Remote."
HOMEPAGE="http://libwiimote.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="+force +tilt"

DEPEND="net-wireless/bluez"

src_prepare() {
	ebegin "Patching for bluez-4.* support"
	sed -i "s@hci_remote_name@hci_read_remote_name@" src/wiimote_link.c || die "sed failed"
	sed -i "s@hci_remote_name@hci_read_remote_name@" configure.in || die "sed failed"
	eend
	epatch "${FILESDIR}"/${PN}-{amd64,as-needed,include}.patch
	use "tilt" || sed -i "s@-D_ENABLE_TILT@@" config.mk.in
	use "force" || sed -i "s@-D_ENABLE_FORCE@@" config.mk.in
	eautoreconf
}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc AUTHORS NEWS README TODO
}
