# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

EGIT_REPO_URI="git://github.com/ezrec/libp5glove.git"
EGIT_PROJECT="ezrec/libp5glove"

inherit autotools git-2

DESCRIPTION="Essential Reality P5 Data Glove driver library."
HOMEPAGE="http://github.com/ezrec/libp5glove"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="x11-libs/libX11
	media-libs/freeglut
	>=dev-libs/libusb-0.1.7"

src_prepare() {
	./autogen.sh
}

src_configure() {
	econf --with-x || die "econf failed"
}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	dodoc AUTHORS ChangeLog README
}
