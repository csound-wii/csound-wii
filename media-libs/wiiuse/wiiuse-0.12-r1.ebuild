# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils toolchain-funcs

MY_P=${P/-/_v}
DESCRIPTION="Wiiuse is a library written in C that connects with several Nintendo Wii remotes."
HOMEPAGE="http://wiiuse.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${MY_P}_src.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="examples"

DEPEND="net-wireless/bluez
	examples? ( media-libs/libsdl[opengl] )"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	sed -i "s@CC = gcc@CC = $(tc-getCC)@" example/Makefile
	sed -i "s@CC = gcc@CC = $(tc-getCC)@" example-sdl/Makefile
	sed -i "s@CC = gcc@CC = $(tc-getCC)@" src/Makefile
}

src_compile() {
	if use examples; then
		emake -j1
	else
		emake -j1 wiiuse
	fi
}

src_install() {
	dodoc CHANGELOG README
	if use examples; then
		dobin example/release*/wiiuse-example
		dobin example-sdl/release*/wiiuse-sdl
	fi
	insinto /usr/include
	doins src/wiiuse.h
	dolib.so src/release*/libwiiuse.so
}
