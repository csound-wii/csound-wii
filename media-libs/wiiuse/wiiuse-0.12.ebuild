# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
inherit eutils

DESCRIPTION="Wiiuse is a library written in C that connects with several Nintendo Wii remotes."
HOMEPAGE="http://wiiuse.sourceforge.net/"
MY_P=${P/-/_v}
SRC_URI="mirror://sourceforge/${PN}/${MY_P}_src.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="net-wireless/bluez
	media-libs/libsdl[opengl]"

S="${WORKDIR}/${MY_P}"

src_compile() {
	emake
}

src_install() {
	dodoc CHANGELOG README
	dobin example/release*/wiiuse-example
	dobin example-sdl/release*/wiiuse-sdl
	insinto /usr/include
	doins src/wiiuse.h
	insinto /usr/$(get_libdir)/
	insopts -m0755
	doins src/release*/libwiiuse.so
}
