# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils autotools

DESCRIPTION="Synthesis ToolKit in C++"
HOMEPAGE="http://ccrma.stanford.edu/software/stk/"
SRC_URI="http://ccrma.stanford.edu/software/stk/release/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="alsa debug doc jack oss"

RDEPEND="alsa? ( media-libs/alsa-lib )
	jack? ( media-sound/jack-audio-connection-kit )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
	dev-lang/perl"

src_prepare() {
	epatch ${FILESDIR}/${P}-sharedlibs.patch
}

src_configure() {
	econf \
		$(use_with jack) \
		$(use_with alsa) \
		$(use_with oss) \
		$(use_enable debug) \
		RAWWAVE_PATH=/usr/share/stk/rawwaves/ \
		|| die "configure failed!"
}

src_compile() {
	cd src
	emake || die "make in src failed!"
}

src_install() {
	dodoc README
	dolib src/libstk.*
	insinto /usr/include/stk
	doins include/*.h include/*.msg include/*.tbl
	insinto /usr/share/stk/rawwaves
	doins rawwaves/*.raw
	if use doc; then
		dohtml -r doc/html/*
	fi

	cat <<- EOF > 62stk
	RAWWAVE_PATH=/usr/share/stk/rawwaves/
	EOF

	doenvd 62stk
}
