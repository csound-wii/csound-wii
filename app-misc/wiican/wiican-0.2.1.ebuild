# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
PYTHON_DEPEND="2"

inherit python distutils

DESCRIPTION="Wiimote configuration and management assistant."
HOMEPAGE="https://launchpad.net/wiican"
SRC_URI="http://launchpad.net/wiican/0.2/${PV}/+download/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-misc/cwiid
	dev-python/dbus-python
	dev-python/pygtk
	dev-python/pyyaml
	dev-python/notify-python
	sys-apps/hal"

pkg_setup() {
	python_set_active_version 2
}

src_prepare() {
	python_convert_shebangs -r 2 .
}

src_install() {
	distutils_src_install
	dodoc AUTHORS TODO
}
