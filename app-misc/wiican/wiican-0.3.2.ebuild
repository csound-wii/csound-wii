# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
PYTHON_DEPEND="2"

inherit gnome2-utils python distutils

DESCRIPTION="Wiimote configuration and management assistant."
HOMEPAGE="https://launchpad.net/wiican"
SRC_URI="http://launchpad.net/wiican/0.3/${PV}/+download/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=app-misc/cwiid-20110107-r1
	dev-python/dbus-python
	dev-python/gconf-python
	dev-python/pygobject
	dev-python/pygtksourceview:2
	dev-python/python-gudev
	dev-python/pygtk
	dev-python/pyxdg
	dev-python/ply
	!sys-apps/hal"
#	net-wireless/gnome-bluetooth
#	sys-fs/udev"

S="${WORKDIR}/${PN}"

pkg_setup() {
	python_set_active_version 2
}

src_prepare() {
	python_convert_shebangs -r 2 .
}

src_install() {
	distutils_src_install
	dodoc AUTHORS TODO
	# Already provided by cwiid
	rm ${D}/lib/udev/rules.d/99-uinput.rules
}

pkg_preinst() {
	gnome2_icon_savelist
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
	gnome2_schemas_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	gnome2_schemas_update --uninstall
}
