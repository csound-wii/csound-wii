# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit eutils

DESCRIPTION="Wiimotedev Project consists of wiimotedev daemon and Qt frontend for use with Wii remotes."
HOMEPAGE="http://code.google.com/p/wiimotedev/"
SRC_URI="http://wiimotedev.googlecode.com/files/${PN}-project-${PV}-1.tar.xz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples"

RDEPEND="dev-qt/qtgui:4
	dev-qt/qtdbus:4
	dev-qt/qtopengl:4
	net-wireless/bluez
	>=app-misc/cwiid-20110107-r1"

DEPEND="${RDEPEND}
	dev-util/cmake"

S="${WORKDIR}/${PN}-project"

src_prepare() {
	sed -i "s@opts@extra_started_commands@" init.d/gentoo/wiimotedev-daemon
}

src_configure() {
	local exs="OFF"
	if use examples; then
		exs="ON"
	fi
	cmake \
		-DDISTRO=gentoo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DUSE_STATIC_CWIID=OFF \
		-DBUILD_EXAMPLES=${exs}
}

src_install() {
	make DESTDIR="${D}" install
	dodoc README CHANGELOG
}

pkg_postinst() {
	elog "Please see"
	elog "http://code.google.com/p/clementine-player/wiki/WiiRemotes"
	elog "about instructions for use with media-sound/clementine."
}
