# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit eutils git-2

DESCRIPTION="Wiimotedev Project is to show what really can be done with wiiremotes."
HOMEPAGE="http://gitorious.org/wiimotedev"
EGIT_REPO_URI="git://gitorious.org/wiimotedev/wiimotedev-project.git"
EGIT_PROJECT="${PN}"
EGIT_MASTER="development"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-qt/qtgui:4
	dev-qt/qtdbus:4
	dev-qt/qtopengl:4
	<=net-wireless/bluez-4.96
	>=app-misc/cwiid-20110107-r1"

DEPEND="${RDEPEND}
	dev-util/cmake"

src_configure() {
	cmake \
		-DDISTRO=gentoo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DUSE_STATIC_CWIID=OFF
}

src_install() {
	make DESTDIR="${D}" install
	dodoc README
}

pkg_postinst() {
	elog "Please see"
	elog "http://code.google.com/p/clementine-player/wiki/WiiRemotes"
	elog "about instructions for use with clementine."
}