# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit eutils

DESCRIPTION="Wiimotedev Project is to show what really can be done with wiiremotes."
HOMEPAGE="http://code.google.com/p/wiimotedev/"
SRC_URI="http://wiimotedev.googlecode.com/files/${PN}-project-${PV}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-qt/qtgui:4
	dev-qt/qtdbus:4
	dev-qt/qtopengl:4
	net-wireless/bluez
	>=app-misc/cwiid-20110107-r1"

DEPEND="${RDEPEND}
	dev-util/cmake"

S="${WORKDIR}/${PN}-project-1.3.0"

src_configure() {
	cmake \
		-DDISTRO=gentoo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DUSE_STATIC_CWIID=OFF \
		-DBUILD_TOOLKIT=OFF \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_QWIIMOTEDEV=OFF
}

src_install() {
	make DESTDIR="${D}" install
	dodoc README
}

pkg_postinst() {
	elog "Please see"
	elog "http://code.google.com/p/clementine-player/wiki/WiiRemotes"
	elog "about instructions for use with clementine."
}