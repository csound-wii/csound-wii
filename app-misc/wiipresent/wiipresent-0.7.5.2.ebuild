# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

DESCRIPTION="Use Wiimote for giving presentations using Open Office, xpdf, evince or Acrobat Reader."
HOMEPAGE="http://dag.wieers.com/home-made/wiipresent/"
SRC_URI="http://dag.wieers.com/home-made/wiipresent/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="x11-libs/libX11
	x11-libs/libXtst
	net-wireless/bluez
	media-libs/libwiimote"

src_compile() {
	emake
}

src_install() {
	einstall
	dodoc AUTHORS ChangeLog README TODO
}
