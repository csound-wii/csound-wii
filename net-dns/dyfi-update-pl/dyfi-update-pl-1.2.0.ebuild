# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="A daemon for updating Finnish DynDNS service dy.fi hostnames automatically."
HOMEPAGE="http://www.dy.fi"
SRC_URI="http://www.dy.fi/files/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="dev-lang/perl"

src_install() {
	dobin dyfi-update.pl
	doinitd $FILESDIR/dyfi-update
	insinto /etc
	doins dyfi-update.conf
	dodoc README
}

pkg_postinst() {
	elog "Please edit /etc/dyfi-update.conf before starting the"
	elog "daemon via '/etc/init.d/dyfi-update start'"
	elog "NOTE: This service works only from Finnish IP addresses."
}